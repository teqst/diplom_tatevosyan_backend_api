from django.db.models import Q
from rest_framework.decorators import api_view
from django.shortcuts import render, Http404

from rest_framework.views import APIView
from rest_framework.response import Response

from .models import Product, Category, News, Partner
from .serializers import ProductSerializer, CategorySerializer, NewsSerializer, PartnersSerializer, AllCategoriesSerializer

# Create your views here.

class AllPartnersList(APIView):
    def get(self, request, format=None):
        partners = Partner.objects.all()
        serializer = PartnersSerializer(partners, many=True)
        return Response(serializer.data)

class AllNewsList(APIView):
    def get(self, request, format=None):
        news = News.objects.all()
        serializer = NewsSerializer(news, many=True)
        return Response(serializer.data)

class AllCategoriesList(APIView):
    def get(self, request, format=None):
        categories = Category.objects.all()
        serializer = AllCategoriesSerializer(categories, many=True)
        return Response(serializer.data)

class LatestProductsList(APIView):
    def get(self, request, format=None):
        products = Product.objects.all()[0:4]
        serializer = ProductSerializer(products, many=True)
        return Response(serializer.data)

class ProductDetail(APIView):
    def get_object(self, category_slug, product_slug):
        try:
            return Product.objects.filter(category__slug=category_slug).get(slug=product_slug)
        except Product.DoesNotExist:
            raise Http404
    def get(self, request, category_slug, product_slug, format=None):
        product = self.get_object(category_slug, product_slug)
        serializer = ProductSerializer(product)
        return Response(serializer.data)

class CategoryDetail(APIView):
    def get_object(self, category_slug):
        try:
            return Category.objects.get(slug=category_slug)
        except Product.DoesNotExist:
            raise Http404

    def get(self, request, category_slug, format=None):
        category = self.get_object(category_slug)
        serializer = CategorySerializer(category)
        return Response(serializer.data)


@api_view(['GET'])
def search(request):
    query = request.query_params.get('query', '')

    if query:
        products = Product.objects.filter(Q(name__icontains=query) | Q(about__icontains=query))
        serializer = ProductSerializer(products, many=True)
        return Response(serializer.data)
    else:
        return Response({'products': []})


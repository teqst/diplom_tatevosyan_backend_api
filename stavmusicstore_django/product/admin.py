from django.contrib import admin

from .models import Category, Product, News, Partner

# Register your models here.
admin.site.register(Category)
admin.site.register(Product)
admin.site.register(News)
admin.site.register(Partner)

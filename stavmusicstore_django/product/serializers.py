from rest_framework import serializers

from .models import Category, Product, News, Partner


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = (
            'id',
            'name',
            'get_absolute_url',
            'short_description',
            'about',
            'specification',
            'strings_count',
            'lads_count',
            'available',
            'brand',
            'video_url',
            'price',
            'get_image',
            'get_thumbnail',
            'category',
        )


class AllCategoriesSerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)

    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'slug',
            'get_absolute_url',
            'products'
        )


class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = (
            'id',
            'name',
            'text',
            'topic',
            'date_added',
            'news_video_url',
            'news_picture_url'
        )


class PartnersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Partner
        fields = (
            'id',
            'name',
            'text',
            'partner_picture_url'
        )


class CategorySerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)

    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'slug',
            'products',
            'get_absolute_url',
        )
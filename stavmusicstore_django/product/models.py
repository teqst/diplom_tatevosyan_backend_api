from django.db import models
from django.core.files import File

from io import BytesIO
from PIL import Image

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField()

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return f'/{self.slug}/'


class News(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField()
    news_video_url = models.CharField(max_length=255, blank=True, null=True)
    news_picture_url = models.CharField(max_length=255, blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    topic = models.CharField(max_length=20)
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return f'/{self.slug}/'


class Partner(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField()
    partner_picture_url = models.CharField(max_length=255, blank=True, null=True)
    text = models.CharField(max_length=255)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return f'/{self.slug}/'


class Product(models.Model):
    category = models.ForeignKey(Category, related_name='products', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    video_url = models.CharField(max_length=255, blank=True, null=True)
    brand = models.CharField(max_length=100, blank=True, null=True)
    color = models.CharField(max_length=100, blank=True, null=True)
    material = models.CharField(max_length=100, blank=True, null=True)
    available = models.BooleanField(default=True)
    slug = models.SlugField()
    short_description = models.TextField(blank=True, null=True)
    about = models.TextField(blank=True, null=True)
    specification = models.TextField(blank=True, null=True, default="<ul><li></li></ul>")
    price = models.DecimalField(max_digits=6, decimal_places=2)
    strings_count = models.IntegerField(blank=True, null=True)
    lads_count = models.IntegerField(blank=True, null=True)
    image = models.ImageField(upload_to='uploads/', blank=True, null=True)
    thumbnail = models.ImageField(upload_to='uploads/', blank=True, null=True)
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-date_added',)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return f'/{self.category.slug}/{self.slug}/'

    def get_image(self):
        if self.image:
            return 'http://127.0.0.1:8000' + self.image.url
        return ''

    def get_thumbnail(self):
        if self.thumbnail:
            return 'http://127.0.0.1:8000' + self.thumbnail.url
        else:
            if self.image:
                self.thumbnail = self.make_thumbnail(self.image)
                self.save()

                return 'http://127.0.0.1:8000' + self.thumbnail.url
            else:
                return ''

    def make_thumbnail(self, image, size=(300, 200)):
        img = Image.open(image)
        img.convert('RGB')
        img.thumbnail(size)

        thumb_io = BytesIO()
        img.save(thumb_io, 'JPEG', quality=85)

        thumbnail = File(thumb_io, name=image.name)

        return thumbnail

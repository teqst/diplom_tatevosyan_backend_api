from django.urls import path

from server import views

urlpatterns = [
    path('checkout/', views.checkout),
    path('orders/', views.OrdersList.as_view()),
    path('consultation_request/', views.consultation_request),
]